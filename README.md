# zemoga-dev-test

Zemoga messaging app.

This app is intended to be reviewed by Zemoga's dev and selection teams.
Inside the demo folder you download a video of the app.

The architecture used for this app is Flux, using Redux library to accomplish it.

## Installation

Clone this repo.

This app was made using the Expo SDK.
You can get it running:

```bash
npm install -g expo-cli
```

Then, run
```bash
npm install
```
inside the folder to install all the used dependencies

Then, go inside the project and run

```bash
expo start
```

And follow the steps.

## Support

It supports both iOS and Android, for more info, please check:
[Expo Official Web](https://expo.io/)

## Third Party Libraries

- axios: Used for networking. Used to fetch the data from the Posts and Users API.
- react-native-floating-action: Renders a FAB into the Android Specific component, used to delete all the posts.
- react-native-log-level: Used to log data in dev. environment.
- react-native-size-matters: Used to create custom stylesheets in order to support multiple screens sizes and densities.
- react-native-tab-view: Used to show a Tab View selector to choose between Posts and Favorites in the Android Specific component.
- redux: Used to define a global state of the app, acording to the architecture
- redux-thunk: Redux middleware.
- react-redux: Redux bindings for React

## Missing functionalities

- Internal Storage/Cache 
- Animate deletion
- Unit tests

- HOC's: Not in the list, but I wanted to use some :p

## Thanks for letting me apply to your process!

