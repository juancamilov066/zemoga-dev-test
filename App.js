import React from 'react';
import { Provider } from 'react-redux';
import { RootNavigator } from './src/navigation/RootNavigator';
import { createAppContainer } from 'react-navigation';
import store from './src/redux/store';

const NavigationContainer = createAppContainer(RootNavigator);

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <NavigationContainer />
            </Provider>            
        );
    }
}