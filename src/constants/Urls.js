export default class Urls {
    static BASE_URL = 'https://jsonplaceholder.typicode.com'
    static FETCH_POSTS = `${Urls.BASE_URL}/posts`;

    static POST_DETAIL = id => {
        return `${Urls.BASE_URL}/posts/${id}`;
    }

    static POST_COMMENTS = id => {
        return `${Urls.BASE_URL}/comments?postId=${id}`;
    }

    static USER_INFO = id => {
        return `${Urls.BASE_URL}/users/${id}`;
    }

}