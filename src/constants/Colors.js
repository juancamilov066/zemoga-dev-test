export const Colors = {
    primary: "#00CE79",
    black: "#000000",
    white: "#FFFFFF",
    light: '#54585B',
    accent: '#AF0029'
}