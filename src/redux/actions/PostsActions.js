import Keys from "../../constants/Keys";
import { HttpClient } from "../../utils/HttpClient";
import Urls from "../../constants/Urls";
import { logger } from "../../utils/Logger";

const fetch = () => {
    return {
        type: Keys.POSTS_REDUCER_FETCH_POSTS
    }
}

const deletePost = (post) => {
    return {
        type: Keys.POSTS_REDUCER_DELETE_POST,
        data: post
    }
}

const deletePosts = () => {
    return {
        type: Keys.POSTS_REDUCER_DELETE_POSTS
    }
}

const markAsRead = (post) => {
    return {
        type: Keys.POSTS_REDUCER_MARK_AS_READ,
        data: post
    }
}

const removeFromFavorites = (post) => {
    return {
        type: Keys.POSTS_REDUCER_REMOVE_FROM_FAVS,
        data: post
    }
}

const markAsFavorite = (post) => {
    return {
        type: Keys.POSTS_REDUCER_MARK_AS_FAV,
        data: post
    }
}

const addPosts = (posts) => {
    return {
        type: Keys.POSTS_REDUCER_ADD_POSTS,
        data: posts
    }
}

const error = (error) => {
    return {
        type: Keys.POSTS_REDUCER_ADD_POSTS_ERROR,
        data: error
    }
}

export const removeFromFavoritesAction = (post) => {
    return function(dispatch, getState) {
        dispatch(removeFromFavorites(post));
    }
}

export const markAsFavoriteAction = (post) => {
    return function(dispatch, getState) {
        dispatch(markAsFavorite(post));
    }
}

export const readAction = (post) => {
    return function(dispatch, getState) {
        dispatch(markAsRead(post));
    }
}

export const deletePostsAction = () => {
    return function(dispatch, getState) {
        dispatch(deletePosts());
    }
}

export const deletePostAction = (post) => {
    return function(dispatch, getState) {
        dispatch(deletePost(post));
    }
}

export const fetchAction = () => {
    return function(dispatch, getState) {
        dispatch(fetch());
        HttpClient.get(Urls.FETCH_POSTS)
                .then(response => {
                    if (response.status === Keys.HTTP_SUCCESS) {
                        const posts = response.data;
                        posts.forEach((post, index) => {
                            post.favorite = false;
                            post.status = index < 20 ? Keys.POST_STATUS_UNREAD : Keys.POST_STATUS_READ;
                        });

                        dispatch(addPosts(posts));
                    }
                })
                .catch(exception => {
                    dispatch(error(exception));
                })
    }
}