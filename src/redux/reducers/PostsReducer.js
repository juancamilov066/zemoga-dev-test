import Keys from "../../constants/Keys";
import { logger } from "../../utils/Logger";

const initialState = {
    posts: [],
    favorites: [],
    loading: false,
    success: undefined,
    error: undefined
}

export const PostsReducer = (state = initialState, action) => {
    const type = action.type;
    if (type === Keys.POSTS_REDUCER_FETCH_POSTS) {
        return {
            ...state,
            success: undefined,
            error: undefined,
            loading: true
        }
    } else if (type === Keys.POSTS_REDUCER_ADD_POSTS) {
        return {
            ...state,
            posts: [...action.data],
            success: true,
            loading: false,
            error: undefined
        }
    } else if (type === Keys.POSTS_REDUCER_ADD_POSTS_ERROR) {
        return {
            ...state,
            loading: false,
            success: false,
            error: action.data
        }
    } else if (type ===  Keys.POSTS_REDUCER_MARK_AS_READ) {
        const postToMarkAsRead = action.data;
        const replacementPosts = [...state.posts];
        const index = state.posts.findIndex(post => post.id === postToMarkAsRead.id);
        
        replacementPosts[index] = postToMarkAsRead;

        return {
            ...state,
            posts: replacementPosts
        }
    } else if (type === Keys.POSTS_REDUCER_MARK_AS_FAV) {
        const postToMarkAsFav = action.data;
        const replacementPosts = [...state.posts];
        const index = state.posts.findIndex(post => post.id === postToMarkAsFav.id);
        
        replacementPosts[index] = postToMarkAsFav;

        return {
            ...state,
            posts: replacementPosts,
            favorites: [...state.favorites, postToMarkAsFav]
        }
    } else if (type === Keys.POSTS_REDUCER_REMOVE_FROM_FAVS) {
        const postToMarkAsNoFav = action.data;
        const replacementPosts = [...state.posts];
        const index = state.posts.findIndex(post => post.id === postToMarkAsNoFav.id);
        
        replacementPosts[index] = postToMarkAsNoFav;

        return {
            ...state,
            posts: replacementPosts,
            favorites: state.favorites.filter(post => { return post.id !== postToMarkAsNoFav.id })
        }
    } else if (type === Keys.POSTS_REDUCER_DELETE_POSTS) {
        return {
            ...state,
            posts: [],
            favorites: []
        }
    } else if (type === Keys.POSTS_REDUCER_DELETE_POST) { 
        const currentPosts = [...state.posts]; // make a separate copy of the array
        const currentFavs = [...state.favorites];
        
        const postsIndex = currentPosts.findIndex(post => post.id === action.data.id);
        const favsIndex = currentFavs.findIndex(post => post.id === action.data.id);

        if (postsIndex !== -1) {
            currentPosts.splice(postsIndex, 1);
        }

        if (favsIndex !== -1) {
            currentFavs.splice(favsIndex, 1);
        }

        return {
            ...state,
            favorites: currentFavs,
            posts: currentPosts
        }
    } else {
        return state;
    }
}