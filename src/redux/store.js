import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import { PostsReducer } from "./reducers/PostsReducer";

const combinedReducers = combineReducers({
    posts: PostsReducer
});

const store = createStore(combinedReducers, applyMiddleware(thunk));
export default store;