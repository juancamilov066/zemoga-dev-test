import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Keys from '../../constants/Keys';
import { Colors } from '../../constants/Colors';

const user = require('../../../assets/images/user.png')

export default class Comment extends PureComponent {

    render() {
        return (
            <View style={CommentStyles.container}>
                <View style={CommentStyles.indicator}>
                    <Image style={CommentStyles.user} source={user} />
                </View> 
                <View style={CommentStyles.commentContainer}>
                    <Text style={CommentStyles.commentMail}>{this.props.comment.email}</Text>
                    <Text style={CommentStyles.comment}>{this.props.comment.body}</Text>
                </View>
            </View>
        );
    }

}

const CommentStyles = ScaledSheet.create({
    container: {
        marginTop: '8@ms',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    indicator: {
        padding: '8@ms'
    },
    user: {
        height: '40@ms',
        width: '40@ms',
        resizeMode: 'cover' 
    },  
    commentContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingLeft: '8@ms'
    },
    commentMail: {
        fontSize: '14@ms',
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_MEDIUM,
        color: Colors.black
    },
    comment: {
        marginTop: '4@ms',
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL,
        color: Colors.light
    }
});
