import React from 'react';
import { View, Text, StyleSheet, Animated, Dimensions, PanResponder, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { Colors } from '../../constants/Colors';
import Keys from '../../constants/Keys';

const { width } = Dimensions.get('window');

export default class ListItem extends React.PureComponent {
    constructor(props) {
        super(props);

        this.gestureDelay = -35;
        this.scrollViewEnabled = true;

        const position = new Animated.ValueXY();
        const panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                const { dx, dy } = gestureState
                return dx > 2 || dx < -2                  
            },
            onPanResponderTerminationRequest: (evt, gestureState) => false,
            onPanResponderMove: (evt, gestureState) => {
                if (gestureState.dx > 35) {
                    this.setScrollViewEnabled(false);
                    let newX = gestureState.dx + this.gestureDelay;
                    position.setValue({ x: newX, y: 0 });
                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                if (gestureState.dx < 150) {
                    Animated.timing(this.state.position, {
                        toValue: { x: 0, y: 0 },
                        duration: 150,
                    }).start(() => {
                        this.setScrollViewEnabled(true);
                    });
                } else {
                    Animated.timing(this.state.position, {
                        toValue: { x: width, y: 0 },
                        duration: 300,
                    }).start(() => {
                        this.props.onDelete(this.props.post);
                        this.setScrollViewEnabled(true);
                    });
                }
            },
        });

        this.panResponder = panResponder;
        this.state = { position };
    }

    setScrollViewEnabled(enabled) {
        if (this.scrollViewEnabled !== enabled) {
            this.props.setScrollEnabled(enabled);
            this.scrollViewEnabled = enabled;
        }
    }

    render() {
        return (
            <View style={PostStyles.container}>
                <Animated.View style={[this.state.position.getLayout()]} {...this.panResponder.panHandlers}>
                    <TouchableOpacity onPress={() => this.props.onPostPressed(this.props.post)}>
                        <View style={PostStyles.absoluteCell}>
                            <Text style={PostStyles.absoluteCellText}>DELETE</Text>
                        </View>
                        <View style={PostStyles.card}>
                            { this.props.post.status === Keys.POST_STATUS_UNREAD && <View style={PostStyles.readIndicator}/> }
                            <View style={PostStyles.titleContainer}>
                                <Text style={PostStyles.titleHeader}>Title</Text>
                                <Text style={this.props.post.status === Keys.POST_STATUS_UNREAD ? PostStyles.unreadTitle : PostStyles.title}>{this.props.post.title}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        );
    }
}

const PostStyles = ScaledSheet.create({
    container: {
        paddingLeft: '10@ms',
        paddingRight: '10@ms',
        marginLeft: -100,
    },
    absoluteCell: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        width: 100,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    absoluteCellText: {
        margin: 16,
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL,
        color: Colors.accent,
    },
    readIndicator: {
        height: '15@ms',
        width: '15@ms',
        marginLeft: '8@ms',
        borderRadius: '15@ms',
        backgroundColor: Colors.primary
    },
    unreadTitle: {
        fontSize: '16@ms',
        flex: 1,
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_MEDIUM,
    },
    title: {
        fontSize: '16@ms',
        flex: 1,
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_REGULAR,
        color: Colors.light
    },
    titleHeader: {
        fontSize: '20@ms',
        flex: 1,
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL,
        textDecorationLine: 'underline'
    },
    card: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 1,
        paddingTop: '18@ms',
        paddingBottom: '18@ms',
        borderRadius: '10@ms',
        backgroundColor: Colors.white,
        marginTop: '4@vs',
        marginBottom: '4@vs',
        alignItems: 'center',
        shadowColor: Colors.light,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginLeft: 100,
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: '16@ms',
        marginRight: '8@ms',
    }
});