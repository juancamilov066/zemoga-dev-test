/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import ListItem from './Post';
import Keys from '../../constants/Keys';

export default class SwipeableList extends Component {

    constructor(props) {
        super(props);

        this.setScrollEnabled = this.setScrollEnabled.bind(this);

        this.state = {
            enable: true,
        };
    }

    setScrollEnabled(enable) {
        this.setState({
            enable,
        });
    }

    onPostPressed = (post) => {
        this.props.onPostPressed(post);
    }

    onDelete = (deletedItem) => {
        this.props.onDelete(deletedItem);
    }

    renderItem(item) {
        return (
            <ListItem
                onPostPressed={this.onPostPressed}
                onDelete={this.onDelete}
                post={item}
                setScrollEnabled={enable => this.setScrollEnabled(enable)}
            />
        );
    }

    render() {
        return (
            <FlatList
                keyExtractor={(item, index) => `${item.id}`}
                style={this.props.style}
                data={this.props.data}
                renderItem={({ item }) => this.renderItem(item)}
                scrollEnabled={this.state.enable}
            />
        );
    }
}

const styles = StyleSheet.create({
    separatorViewStyle: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    separatorStyle: {
        height: 1,
        backgroundColor: '#000',
    },
});