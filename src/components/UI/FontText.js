import React, {Component} from 'react';
import { Text } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

class FontText extends Component {
    
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={this.fontTextStyle.text}>{this.props.children}</Text>
        )
    }

    fontTextStyle = ScaledSheet.create({
        text: {
            ...this.props.style,
            alignItems: 'center',
            fontFamily: this.props.fontFamily
        }
    })

}

export default FontText;