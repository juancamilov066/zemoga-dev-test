import React, { Component } from 'react';
import { SegmentedControlIOS, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux';
import { Colors } from '../../constants/Colors';
import PostsComponent from '../posts/PostsComponent';
import FavoritesComponent from '../favorites/FavoritesComponent';
import Keys from '../../constants/Keys';
import { ScaledSheet } from 'react-native-size-matters';
import { fetchAction } from '../../redux/actions/PostsActions';

const mapStateToProps = state => ({
    posts: state.posts
});

class MainComponent extends Component {

    static navigationOptions = ({ navigation, navigationOptions }) => {

        const fetchPosts = navigation.getParam('fetchPosts', undefined);

        return {
            title: 'Posts',
            headerStyle: {
                backgroundColor: Colors.primary,
                elevation: 0,
                borderBottomWidth: 0
            },
            headerTitleStyle: {
                fontWeight: 'normal',
                fontFamily: Keys.FONT_FAMILY_QUESTRIAL
            },
            headerRight: <TouchableOpacity onPress={() => fetchPosts()}>
                <Image style={MainComponentStyles.reload} source={require('../../../assets/images/reload.png')}/>
            </TouchableOpacity>,
            headerTintColor: Colors.white,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                'POSTS',
                'FAVORITES'
            ]
        }
        
        this.props.navigation.setParams({fetchPosts: this.fetchPosts});
    }

    fetchPosts = () => {
        this.props.dispatch(fetchAction());
    }

    FirstRoute = () => (
        <PostsComponent navigation={this.props.navigation}/>
    );
    
    SecondRoute = () => (
        <FavoritesComponent navigation={this.props.navigation}/>
    );

    render() {
        return (
            <View style={MainComponentStyles.container}>
                <View style={MainComponentStyles.segmentContainer}>
                    <SegmentedControlIOS
                        values={this.state.routes}
                        tintColor={Colors.primary}
                        selectedIndex={this.state.index}
                        onChange={(event) => {
                            this.setState({index: event.nativeEvent.selectedSegmentIndex});
                        }}
                    />
                </View>
                { this.state.index === 0
                    ? this.FirstRoute() 
                    : this.SecondRoute()}
            </View>
        );
    }

}

const MainComponentStyles = ScaledSheet.create({
    segmentContainer: {
        padding: '8@ms'
    },
    container: {
        flex: 1, width: '100%', flexDirection: 'column'
    },
    reload: {
        height: '25@ms',
        width: '25@ms',
        marginRight: '8@ms',
        resizeMode: 'cover'
    }
})

export default connect(mapStateToProps)(MainComponent);