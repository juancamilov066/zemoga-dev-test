import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dimensions, Text, TouchableOpacity, View, Image } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Colors } from '../../constants/Colors';
import PostsComponent from '../posts/PostsComponent';
import FavoritesComponent from '../favorites/FavoritesComponent';
import Keys from '../../constants/Keys';
import { fetchAction } from '../../redux/actions/PostsActions';
import { ScaledSheet } from 'react-native-size-matters';

const mapStateToProps = state => ({
    posts: state.posts
});

class MainComponent extends Component {

    static navigationOptions = ({ navigation, navigationOptions }) => {

        const fetchPosts = navigation.getParam('fetchPosts', undefined);

        return {
            title: 'Posts',
            headerStyle: {
                backgroundColor: Colors.primary,
                elevation: 0,
                borderBottomWidth: 0
            },
            headerTitleStyle: {
                fontWeight: 'normal',
                fontFamily: Keys.FONT_FAMILY_QUESTRIAL
            },
            headerRight: <TouchableOpacity onPress={() => fetchPosts()}>
                <Image style={MainStyles.reload} source={require('../../../assets/images/reload.png')}/>
            </TouchableOpacity>,
            headerTintColor: Colors.white,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                { key: 'all', title: 'All' },
                { key: 'favorites', title: 'Favorites' },
            ]
        }

        this.props.navigation.setParams({fetchPosts: this.fetchPosts});
    }

    fetchPosts = () => {
        this.props.dispatch(fetchAction());
    }

    FirstRoute = () => (
        <PostsComponent navigation={this.props.navigation}/>
    );
    
    SecondRoute = () => (
        <FavoritesComponent navigation={this.props.navigation}/>
    );

    render() {
        return (
            <View style={MainStyles.container}>
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        all: this.FirstRoute,
                        favorites: this.SecondRoute,
                    })}
                    renderTabBar={props =>
                        <TabBar
                            {...props}
                            indicatorStyle={{ backgroundColor: Colors.primary }}
                            style={{ backgroundColor: Colors.primary, elevation: 0, color: Colors.black }}
                        />
                    }
                    renderLabel={({ route, focused, color }) => (
                        <Text style={{ color: Colors.black, 
                            margin: 8}}>
                                {route.title}
                        </Text>
                    )}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                />
            </View>
        );
    }

}

const MainStyles = ScaledSheet.create({
    container: {
        flex: 1, width: '100%'
    },
    reload: {
        height: '25@ms',
        width: '25@ms',
        marginRight: '8@ms',
        resizeMode: 'cover'
    }
})

export default connect(mapStateToProps)(MainComponent);