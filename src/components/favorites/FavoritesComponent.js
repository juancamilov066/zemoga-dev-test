import React, { Component } from 'react';
import { connect } from 'react-redux';
import Post from '../UI/Post';
import { View, Text, Image, FlatList } from 'react-native';
import { FavoritesStyles } from './FavoritesStyles';

const mapStateToProps = state => ({
    posts: state.posts
})

class FavoritesComponent extends Component {

    constructor(props) {
        super(props);
    }

    onPostPressed = (post) => {
        /*if (post.status === Keys.POST_STATUS_UNREAD) {
            post.status = Keys.POST_STATUS_READ;
            this.props.dispatch(readAction(post));
        }*/
        this.props.navigation.navigate('Detail', { post });
    }

    render() {
        return (
            <View style={FavoritesStyles.container}>
                { this.props.posts.favorites.length > 0 ? 
                    <FlatList
                        keyExtractor={(item, index) => `${item.id}`}
                        data={this.props.posts.favorites}
                        extraData={this.props.posts}
                        renderItem={(post) => <Post post={post.item} onPostPressed={this.onPostPressed}/>} /> :
                    <View style={FavoritesStyles.notFoundContainer}>
                        <Image style={FavoritesStyles.notFoundImage} source={require('../../../assets/images/lace/lace.png')}/>
                        <Text style={FavoritesStyles.notFoundTitle}>There's Nothing Here</Text>
                        <Text style={FavoritesStyles.notFoundContent}>You need to mark some POSTS as Favorites</Text>
                    </View>}
            </View>
        )
    }

}

export default connect(mapStateToProps)(FavoritesComponent);