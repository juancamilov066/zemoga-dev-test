import { ScaledSheet } from "react-native-size-matters";
import { Colors } from "../../constants/Colors";
import Keys from "../../constants/Keys";

export const FavoritesStyles = ScaledSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: Colors.white
    },
    notFoundContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    notFoundImage: {

    },
    notFoundTitle: {
        marginTop: '16@ms',
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL,
        fontSize: '20@ms'
    },
    notFoundContent: {
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_REGULAR,
        fontSize: '16@ms',
        color: Colors.light
    }
})