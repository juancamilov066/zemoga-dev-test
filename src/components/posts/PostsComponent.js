import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Platform, TouchableOpacity, Image, Text } from 'react-native';
import { FloatingAction } from 'react-native-floating-action';
import { fetchAction, readAction, deletePostsAction, deletePostAction } from '../../redux/actions/PostsActions';
import Post from '../UI/Post';
import Keys from '../../constants/Keys';
import { PostStyles } from './PostsStyles';
import { SafeAreaView } from 'react-navigation';
import { Colors } from '../../constants/Colors';
import { logger } from '../../utils/Logger';
import SwipeableList from '../UI/SwipeableFlatList';

const mapStateToProps = state => ({
    posts: state.posts
});

const actions = [{
    text: 'Delete Posts',
    icon: require('../../../assets/images/trash.png'),
    name: 'delete_posts',
    color: Colors.accent,
    position: 1
}]

class PostsComponent extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.posts.posts.length === 0) {
            this.fetchPosts();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.login !== this.props.login) {

        }
    }

    fetchPosts = () => {
        this.props.dispatch(fetchAction());
    }

    onDeletePressed = () => {
        this.props.dispatch(deletePostsAction());
    }

    onPostPressed = (post) => {
        if (post.status === Keys.POST_STATUS_UNREAD) {
            post.status = Keys.POST_STATUS_READ;
            this.props.dispatch(readAction(post));
        }
        this.props.navigation.navigate('Detail', { post });
    }

    onDelete = (post) => {
        this.props.dispatch(deletePostAction(post));
    }

    render() {
        return (
            <SafeAreaView style={PostStyles.container}>
                { this.props.posts.posts.length > 0 ? 
                    <View style={PostStyles.listContainer}>
                        <SwipeableList style={PostStyles.list}
                            refreshing={this.props.posts.loading}
                            onDelete={this.onDelete}
                            onRefresh={this.fetchPosts}
                            data={this.props.posts.posts}
                            onPostPressed={this.onPostPressed}
                            extraData={this.props.posts}
                            renderItem={(post) => <Post post={post.item} />}  />
                        { Platform.OS === 'ios' && this.props.posts.posts.length > 0 && <TouchableOpacity onPress={this.onDeletePressed} style={PostStyles.deleteButtonContainer}>
                            <Text style={PostStyles.deleteButtonText}>DELETE POSTS</Text>
                        </TouchableOpacity> }
                    </View> :
                    <View style={PostStyles.notFoundContainer}>
                        <Image style={PostStyles.notFoundImage} source={require('../../../assets/images/tumbleweed/tumbleweed.png')}/>
                        <Text style={PostStyles.notFoundTitle}>It's really lonely here</Text>
                        <TouchableOpacity onPress={this.fetchPosts}><Text style={PostStyles.notFoundContent}>Click here to download the posts</Text></TouchableOpacity>
                    </View> }
                { Platform.OS === 'android' && <FloatingAction
                    visible={this.props.posts.posts.length > 0}
                    color={Colors.accent}
                    actions={actions}
                    onPressItem={
                        (name) => {
                            if (name === 'delete_posts') {
                                this.onDeletePressed();
                            }
                        }
                    }
                /> }
            </SafeAreaView>
        );
    }


}

export default connect(mapStateToProps)(PostsComponent);