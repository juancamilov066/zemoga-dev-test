import { ScaledSheet } from "react-native-size-matters";
import { Colors } from "../../constants/Colors";
import Keys from "../../constants/Keys";

export const PostStyles = ScaledSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        backgroundColor: Colors.white
    },
    listContainer: {
        flex: 1,
        flexWrap: 'wrap'
    }, 
    list: {
        flex: 1
    },
    deleteButtonContainer: {
        width: '100%',
        backgroundColor: Colors.accent,
        padding: '10@ms',
        alignItems: 'center',
        justifyContent: 'center'
    },
    deleteButtonText: {
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_MEDIUM,
        color: Colors.white
    },  
    notFoundContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    notFoundImage: {

    },
    notFoundTitle: {
        marginTop: '16@ms',
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL,
        fontSize: '20@ms'
    },
    notFoundContent: {
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_REGULAR,
        fontSize: '16@ms',
        color: Colors.light
    }
})