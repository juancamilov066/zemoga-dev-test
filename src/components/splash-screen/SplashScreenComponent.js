import React, { PureComponent } from 'react';
import { Font } from 'expo';
import { View, Image } from 'react-native';
import { SplashScreenStyles } from './SplashScreenStyles';

const zemoga = require('../../../assets/zemoga.jpeg');

class SplashScreenComponent extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            fontsLoaded: false
        }
    }

    componentWillMount() {
        this.loadFonts();
    }

    async loadFonts() {
        await Font.loadAsync({
            'Montserrat-Medium': require('../../../assets/fonts/Montserrat-Medium.ttf'),
            'Questrial-Regular': require('../../../assets/fonts/Questrial-Regular.ttf'),
            'Montserrat-Regular': require('../../../assets/fonts/Montserrat-Regular.ttf'),
        });
        this.setState({fontsLoaded: true}, () => {
            setTimeout(() => {
                this.props.navigation.replace('Main');
            }, 3000);
        });
    }

    render() {
        return (
            <View style={SplashScreenStyles.container}>
                <Image style={SplashScreenStyles.zemoga} source={zemoga}/>
            </View>
        );
    }

}

export default SplashScreenComponent;