import { ScaledSheet } from "react-native-size-matters";
import { Colors } from "../../constants/Colors";

export const SplashScreenStyles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.black,
        alignItems: 'center',
        justifyContent: 'center'
    },
    zemoga: {
        height: '150@ms',
        width: '150@ms',
        resizeMode: 'cover'
    }
})