import { ScaledSheet } from "react-native-size-matters";
import { Colors } from "../../constants/Colors";
import Keys from "../../constants/Keys";

export const PostDetailStyles = ScaledSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: Colors.white
    },
    descriptionView: {
        padding: '20@ms',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.primary
    },
    descriptionHeader: {
        fontSize: '20@ms',
        color: Colors.white,
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_MEDIUM,
        textDecorationLine: 'underline'
    },
    description: {
        fontSize: '16@ms',
        color: Colors.white,
        marginTop: '10@vs',
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL
    }, 
    userViewContainer: {
        backgroundColor: Colors.white,
        width: '100%',
        minHeight: '250@ms',
        flexDirection: 'row'
    },
    profilePic: {
        flex: 0.4,
        borderBottomRightRadius: '10@ms',
        borderTopRightRadius: '10@ms',
        height: '100%',
        maxHeight: '250@ms',
        resizeMode: 'cover' 
    }, 
    infoContainer: {
        padding: '8@ms',
        flex: 0.6,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    infoTextContainer: {
        flexDirection: 'column',
        marginLeft: '8@ms'
    },  
    infoItem: {
        alignItems: 'center',
        marginTop: '8@ms',
        flexDirection: 'row'
    },
    infoIcon: {
        height: '20@ms',
        width: '20@ms',
        resizeMode: 'cover'
    },
    infoTitle:{
        fontSize: '12@ms',
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_MEDIUM,
        color: Colors.black
    },
    info: {
        fontFamily: Keys.FONT_FAMILY_QUESTRIAL,
        color: Colors.light
    },
    star: {
        height: '25@ms',
        width: '25@ms',
        marginRight: '8@ms',
        resizeMode: 'cover'
    }, 
    commentsTitle: {
        padding: '16@ms',
        fontSize: '20@ms',
        color: Colors.black,
        fontFamily: Keys.FONT_FAMILY_MONTSERRAT_MEDIUM,
        textDecorationLine: 'underline'
    },
    commentsContainer: {
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})