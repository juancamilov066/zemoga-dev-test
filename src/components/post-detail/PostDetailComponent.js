import React, { Component } from 'react';
import UsersService from '../../services/UsersService';
import { connect } from 'react-redux';
import { ScrollView, View, Text, Image, TouchableOpacity} from 'react-native';
import { PostsService } from '../../services/PostsService';
import { logger } from '../../utils/Logger';
import { Colors } from '../../constants/Colors';
import { markAsFavoriteAction, removeFromFavoritesAction } from '../../redux/actions/PostsActions';
import { PostDetailStyles } from './PostDetailStyles';
import Keys from '../../constants/Keys';
import Comment from '../UI/Comment';

const mapStateToProps = state => ({
    posts: state.posts
});

const web = require('../../../assets/images/web.png');
const name = require('../../../assets/images/name.png');
const phone = require('../../../assets/images/phone.png');
const mail = require('../../../assets/images/mail.png');

class PostDetailComponent extends Component {

    static navigationOptions = ({ navigation, navigationOptions }) => {

        const post = navigation.getParam('post', undefined);
        const markPostAsFavorite = navigation.getParam('markPostAsFavorite', undefined);

        return {
            title: 'Detail',
            headerStyle: {
                backgroundColor: Colors.primary,
                elevation: 0
            },
            headerTitleStyle: {
                fontWeight: 'normal',
                fontFamily: Keys.FONT_FAMILY_QUESTRIAL
            },
            headerRight: <TouchableOpacity onPress={() => markPostAsFavorite(post)}>
                { post.favorite === false 
                ? <Image style={PostDetailStyles.star} source={require('../../../assets/images/empty-star.png')} /> 
                : <Image style={PostDetailStyles.star} source={require('../../../assets/images/star.png')}/> }
            </TouchableOpacity>,
            headerTintColor: Colors.white,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            post: props.navigation.getParam('post', undefined),
            user: undefined,
            comments: undefined
        }

        this.userService = new UsersService();
        this.postService = new PostsService();
        
        this.props.navigation.setParams({markPostAsFavorite: this.markPostAsFavorite});
    }

    componentDidMount() {
        const { post } = this.state;
         if (post !== undefined) {
            Promise.all([
                this.userService.findById(post.userId),
                this.postService.findPostComments(post.id)
            ]).then(resolution => {
                this.setState({
                    user: resolution[0], 
                    comments: resolution[1]
                });
            })
            .catch(exception => {
                logger.error(exception);
            });
        } else {

        }
    }

    markPostAsFavorite = (post) => {
        if (post.favorite === false) {
            post.favorite = true;
            this.props.dispatch(markAsFavoriteAction(post));
        } else {
            post.favorite = false;
            this.props.dispatch(removeFromFavoritesAction(post));
        }
        this.props.navigation.setParams({ post });
    }

    render() {
        return (
            <ScrollView style={PostDetailStyles.container}>
                <View style={PostDetailStyles.descriptionView}>
                    <Text style={PostDetailStyles.descriptionHeader}>TITLE</Text>
                    <Text style={PostDetailStyles.description}>{this.state.post.title}</Text>
                </View>
                <View style={PostDetailStyles.descriptionView}>
                    <Text style={PostDetailStyles.descriptionHeader}>DESCRIPTION</Text>
                    <Text style={PostDetailStyles.description}>{this.state.post.body}</Text>
                </View>
                { this.state.user !== undefined && <View style={PostDetailStyles.userViewContainer}>
                    <Image style={PostDetailStyles.profilePic} source={require('../../../assets/images/profile.jpg')}></Image>
                    <View style={PostDetailStyles.infoContainer}>
                        <View style={PostDetailStyles.infoItem}>
                            <Image source={name} style={PostDetailStyles.infoIcon} />
                            <View style={PostDetailStyles.infoTextContainer}>
                                <Text style={PostDetailStyles.infoTitle}>Name</Text>
                                <Text style={PostDetailStyles.info}>{this.state.user.name}</Text>
                            </View>
                        </View>
                        <View style={PostDetailStyles.infoItem}>
                            <Image source={mail} style={PostDetailStyles.infoIcon} />
                            <View style={PostDetailStyles.infoTextContainer}>
                                <Text style={PostDetailStyles.infoTitle}>Email</Text>
                                <Text style={PostDetailStyles.info}>{this.state.user.email}</Text>
                            </View>
                        </View>
                        <View style={PostDetailStyles.infoItem}>
                            <Image source={phone} style={PostDetailStyles.infoIcon} />
                            <View style={PostDetailStyles.infoTextContainer}>
                                <Text style={PostDetailStyles.infoTitle}>Phone</Text>
                                <Text style={PostDetailStyles.info}>{this.state.user.phone}</Text>
                            </View>
                        </View>
                        <View style={PostDetailStyles.infoItem}>
                            <Image source={web} style={PostDetailStyles.infoIcon} />
                            <View style={PostDetailStyles.infoTextContainer}>
                                <Text style={PostDetailStyles.infoTitle}>Web</Text>
                                <Text style={PostDetailStyles.info}>{this.state.user.website}</Text>
                            </View>
                        </View>
                    </View>
                </View> }
                { this.state.comments && <View style={PostDetailStyles.commentsContainer}>
                    <Text style={PostDetailStyles.commentsTitle}>COMMENTS</Text>
                    { this.state.comments.map(comment => {
                        return <Comment key={comment.id} comment={comment} />
                    }) }
                </View> }
            </ScrollView>
        )
    }

}

export default connect(mapStateToProps)(PostDetailComponent);