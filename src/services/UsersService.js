import { HttpClient } from "../utils/HttpClient";
import Urls from "../constants/Urls";
import Keys from "../constants/Keys";

export default class UsersService {

    findById = (id) => {
        return new Promise((resolve, reject) => {
            HttpClient.get(Urls.USER_INFO(id))
                .then(response => {
                    if (response.status === Keys.HTTP_SUCCESS) {
                        resolve(response.data);
                    }
                })
                .catch(exception => {
                    reject(exception);
                })
        });
    }

}