import { HttpClient } from "../utils/HttpClient";
import Urls from "../constants/Urls";
import { logger } from "../utils/Logger";
import Keys from "../constants/Keys";

export class PostsService {

    findPostComments = (id) => {
        return new Promise((resolve, reject) => {
            HttpClient.get(Urls.POST_COMMENTS(id))
                .then(response => {
                    if (response.status === Keys.HTTP_SUCCESS) {
                        resolve(response.data);
                    }
                })
                .catch(exception => {
                    console.log(exception);
                    reject(exception);
                });
        })
    }

}