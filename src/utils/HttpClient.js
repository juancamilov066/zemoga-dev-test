import axios from 'axios';
import { logger } from './Logger';

const errorResponseHandler = (error) => {
    if (error.response) {
        return Promise.reject({
            code: error.response.status,
            message: error.response.data.message 
        })
    }

    logger.error(error);
    return Promise.reject({
        code: 500,
        message: 'Unknown Error'
    })
}

axios.defaults.timeout = 10000;
axios.interceptors.response.use(
    response => response,
    errorResponseHandler
);

export const HttpClient = axios;