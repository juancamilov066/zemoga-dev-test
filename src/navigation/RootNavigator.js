import { createStackNavigator } from "react-navigation";
import SplashScreenComponent from "../components/splash-screen/SplashScreenComponent";
import MainComponent from "../components/main/MainComponent";
import PostDetailComponent from "../components/post-detail/PostDetailComponent";

export const RootNavigator = createStackNavigator({
    Splash: {
        screen: SplashScreenComponent,
        navigationOptions: {
            header: null
        }
    },
    Main: {
        screen: MainComponent
    },
    Detail: {
        screen: PostDetailComponent
    }
})